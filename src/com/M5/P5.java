package com.M5;

import java.util.Scanner;

public class P5 {

    private Scanner sc = new Scanner(System.in);

    public void showMenu(){
        System.out.println("Escoge una opcion:");
        System.out.println("1.Hello World");
        System.out.println("0. Salir\n");
    }

    public int readOpcion(){
        return sc.nextInt();
    }

    public void operar(int opcion){
        
        switch (opcion){
            case 0:
                exit();
                break;
            case 1: helloWorld();
                break;
        }
    }

    /* OPERATIONS */
    public void exit(){
        System.out.println("END");
    }
    public void helloWorld(){
        System.out.println("Hello World");
    }


    public static void main(String[] args) {

        P5 p5 = new P5();

        int response = 0;
        do{
            p5.showMenu();
            response = p5.readOpcion();
            p5.operar(response);
        }while(response != 0);

    }
}
